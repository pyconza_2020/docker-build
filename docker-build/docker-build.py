from datetime import datetime
import os, os.path

import attr
import docker
import json
import requests, requests.exceptions
from requests.api import get

import git_utils
import pyproject

ETCD_BASE_URL = "http://etcd.local/v2/keys/docker"
DEFAULT_VERSION = "0.1"
DTR_NAME = "dtr.local"

VERSION = "1.1"


class DockerBuildError(Exception):
    pass


@attr.s
class DockerBuild(object):
    project = attr.ib(default="local")
    name = attr.ib(default=None)
    version = attr.ib(default=None)
    docker_file = attr.ib(default="Dockerfile")
    debug = attr.ib(default=False)

    def __attrs_post_init__(self):
        self.image = False
        self.labels = {"builder.version": VERSION, "build.timestamp": datetime.now().isoformat()}
        try:
            self.toml = pyproject.PyProjectToml(debug=self.debug)
        except pyproject.NoTomlFileError:
            self.toml = None
        self.docker_client = docker.from_env()
        self.get_version()
        self.get_name()
        missing = [a for a in ("project", "name", "version") if not getattr(self, a)]
        if missing:
            raise DockerBuildError(f"These required parameters can not be set: {'; '.join(missing)}")
        self.tag = f"{DTR_NAME}/{self.project}/{self.name}:{self.version}"
        self.set_git_labels()
        self.build()
        if self.project not in ("local", "prod"):
            self.push()
        self.store_version()

    def set_git_labels(self):
        try:
            details = git_utils.get_git_details()
            self.labels["git.commit"] = details.hash
            self.labels["git.working_copy_clean"] = str(details.clean)
            if details.remote:
                self.labels["git.remote_url"] = details.remote
        except git_utils.GitError:
            pass

    def get_version(self):
        if self.version is not None:
            return
        if self.toml:
            self.version = self.toml.version
            if self.debug:
                print(f"Found version {self.version} in PyProject TOML file")
        else:
            try:
                if self.debug:
                    print("Retrieving version from ETCD")
                res = requests.get(f"{ETCD_BASE_URL}/{self.name}", verify=False)
                res.raise_for_status()
                current_version = res.json()["node"]["value"]
                (preamble, last) = current_version.rsplit(".", 1)
                self.version = f"{preamble}.{int(last) + 1}"
                if self.debug:
                    print(f"Found version {current_version} in ETCD. New version set to {self.version}")
            except requests.exceptions.HTTPError:
                if self.debug:
                    print(f"Could not retrieve a version from ETCD, using the default version of {DEFAULT_VERSION}")
                self.version = DEFAULT_VERSION
            except Exception as e:
                if self.debug:
                    print(f"Could not process the version received from ETCD ({current_version}), using the default version of {DEFAULT_VERSION}")
                self.version = DEFAULT_VERSION

    def get_name(self):
        """Set name for image, if not already supplied. Try TOML file then directory path"""
        if self.name is not None:
            return
        if self.toml:
            self.name = self.toml.name
            if self.debug:
                print(f'Found name "{self.name}" in PyProject TOML file')
        else:
            try:
                self.name = os.path.basename(os.getcwd())
                if self.debug:
                    print(f'Extracted name from current directory: "{self.name}"')
            except Exception as e:
                if self.debug:
                    print(f"Could not determine image name")
                self.name = None

    def store_version(self):
        if self.version is None:
            if self.debug:
                print("Error: No version set to store in ETCD")
            return
        requests.put(f"{ETCD_BASE_URL}/{self.name}", data={"value": self.version}, verify=False)
        if self.debug:
            print(f'Stored version {self.version} for image "{self.name}" in ETCD')

    def build(self):
        """Build docker image from specified docker_file, project, name and version attributes"""

        try:
            if self.debug:
                print()
                print("Docker build log:")
            for line in self.docker_client.api.build(
                path=os.getcwd(),
                dockerfile=self.docker_file,
                tag=self.tag,
                quiet=False,
                labels=self.labels,
                rm=True,
                forcerm=True,
                network_mode="host",
            ):
                if self.debug:
                    text = json.loads(line).get("stream", "")
                    if text:
                        print(text, end="")
            self.image = self.docker_client.images.get(self.tag)
        except Exception as e:
            raise DockerBuildError(f"Error: could not build image: {e}")

    def push(self):
        """Push built container, along with a 'latest' tag"""
        if not self.image:
            raise DockerBuildError("Can't push, image has not been successfully built")
        if self.project in ("local", "prod"):
            raise DockerBuildError('Cannot push to "local" or "prod" projects')
        self.image.tag(f"{DTR_NAME}/{self.project}/{self.name}:latest")
        tags = [self.tag, f"{DTR_NAME}/{self.project}/{self.name}:latest"]
        for tag in tags:
            if self.debug:
                print()
                print(f"Pushing {tag}:")
            for line in self.docker_client.images.push(tag, stream=True, decode=True):
                if self.debug and any([c in line.get("status", "") for c in ("Preparing", "Waiting", "Pushed", "digest")] + ["aux" in line]):
                    print(line, end="\n")


if __name__ == "__main__":
    from argparse_helper.argparse_helper import CommandLineParser

    clp = CommandLineParser(
        "Tool to build and push Docker images in the company-standard manner. Images in the local and prod projects are not pushed."
    )
    clp.add_quiet()
    clp.add_argument("--project", default="local", help='The project for the Docker image. Defaults to "%(default)s"')
    clp.add_argument(
        "--name", default=None, help="The name of the Docker image. Will be determined from the PyProject TOML file or directory name if not supplied"
    )
    clp.add_argument(
        "--version", default=None, help="The version of the Docker image. Will be determined from the PyProject TOML file or the ETCD server"
    )
    clp.add_argument("--docker-file", default="Dockerfile", help='The name of the Docker file to process. Defaults to "%(default)s"')
    clp.parse()

    try:
        db = DockerBuild(project=clp.project, name=clp.name, version=clp.version, docker_file=clp.docker_file, debug=clp.include_output)
    except Exception as e:
        if clp.include_output:
            print(e)
