from collections import namedtuple

import git, git.exc


class GitError(Exception):
    pass


class NoGitRepoError(GitError):
    pass


GIT_DETAILS = namedtuple("GitDetails", ("hash", "clean", "remote"))


def get_git_details():
    try:
        repo = git.Repo()
        remote = None
        urls = [u for u in repo.remote().urls]
        if urls:
            remote = urls[0]
        return GIT_DETAILS(repo.head.object.hexsha, not repo.is_dirty(), remote)
    except git.exc.InvalidGitRepositoryError:
        raise NoGitRepoError("No git repo to read")
