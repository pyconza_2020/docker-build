""" Attempt to return project details from pyproject.toml in local directory
"""

import os.path
import toml


TOML_FILE = "pyproject.toml"


class TomlError(Exception):
    pass


class NoTomlFileError(TomlError):
    pass


class PyProjectToml:
    def __init__(self, toml_path=TOML_FILE, debug=False):
        if not os.path.exists(toml_path):
            if debug:
                print("No PyProject TOML file available")
            raise NoTomlFileError
        try:
            with open(toml_path, "r") as fh:
                t = toml.load(toml_path)
            sec = t["tool"]["poetry"]
            self.version = sec.get("version", None)
            self.name = sec.get("name", None)
            self.description = sec.get("description", "")

        except Exception:
            if debug:
                print("Error processing PyProject TOML file")
            raise TomlError()
